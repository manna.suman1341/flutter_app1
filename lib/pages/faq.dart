import 'package:flutter/material.dart';
import 'package:flutterapp1/datasource.dart';
class faqs extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("FAQs"),),
      body: ListView.builder(
        itemCount: DataSource.questionAnswers.length,
          itemBuilder: (context,index){
        return ExpansionTile(
          title: Text(DataSource.questionAnswers[index]['question']),
        children: <Widget>[
          Padding(

            padding: const EdgeInsets.all(10.0),

            child: Text(DataSource.questionAnswers[index]['answer']),
          ),

        ],
        );
      }),
    );
  }
}
