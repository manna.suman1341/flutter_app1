import 'package:flutter/material.dart';
class worldWidePanel extends StatelessWidget {
  final Map WorldData;

  const worldWidePanel({Key key, this.WorldData}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    String covidData(data){
      if(data>=1000000000){
        data = (data/1000000000).toStringAsFixed(2) + " B";
      }else if(data>1000000){
        data = (data/1000000).toStringAsFixed(2) + " M";

      }else if(data>1000){
        data = (data/1000).toStringAsFixed(0) + " K";
      }
      return data;
    }
    return Container(
      child: GridView(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2, childAspectRatio: 2),
        children: <Widget>[
          
          StatusPanel(
            title: 'CONFIRMED',
            panelColor: Colors.red[100],
            textColor: Colors.red,

            count : (WorldData['cases']/1000000).toStringAsFixed(2) + " M",
          ),

          StatusPanel(
              title: 'ACTIVE',
              panelColor: Colors.blue[100],
              textColor: Colors.blue,
              count : covidData(WorldData['active']).toString(),
          ),
          StatusPanel(
              title: 'RECOVERED',
              panelColor: Colors.green[100],
              textColor: Colors.green,
              count : covidData(WorldData['recovered']).toString(),
          ),
          StatusPanel(
              title: 'DEATHS',
              panelColor: Colors.grey[400],
              textColor: Colors.grey[900],
              count : covidData(WorldData['deaths']),
          ),
        ],
      ),
    );
  }
}
class StatusPanel extends StatelessWidget {

  final Color panelColor;
  final Color textColor;
  final String title;
  final String count;

  const StatusPanel({Key key, this.panelColor, this.textColor, this.title, this.count}) : super(key: key);
  @override

  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    return Container(
      margin: EdgeInsets.symmetric(vertical: 2,horizontal: 2),
      height: 40,width: width/2,
      
      decoration: BoxDecoration(
        color: panelColor,
        boxShadow: [
          BoxShadow(
            color: Colors.white,
            blurRadius: 25.0,
            spreadRadius: 5.0,
            offset: Offset(
              15.0,
              15.0,
            )
          )
        ]

      ),

      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          
          Text(title, style: TextStyle(color:textColor,fontWeight: FontWeight.bold, fontSize: 14),),



          Text(count, style: TextStyle(color:textColor,fontSize: 29, fontWeight: FontWeight.bold),)
        ],
      ),
    );
  }
}



