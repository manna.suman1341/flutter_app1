import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutterapp1/datasource.dart';
import 'package:flutterapp1/pages/faq.dart';
import 'package:flutterapp1/pages/helpline.dart';
class infopanel extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(

      child: Column(
        children: <Widget>[
          GestureDetector(
            onTap: (){
              Navigator.push(context,MaterialPageRoute(
                builder: (context) => faqs()
              ));
            },
            child: Container(
              color: primaryBlack,
              padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
              margin: EdgeInsets.symmetric(vertical: 5,horizontal: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text('FAQ',style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 20),),
                  Icon(Icons.arrow_forward,color:Colors.white)

                ],
              ),
            ),
          ),
          Container(
            color: primaryBlack,
            padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
            margin: EdgeInsets.symmetric(vertical: 5,horizontal: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text('ICMR LAB DETAILS',style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 20),),
                Icon(Icons.arrow_forward,color:Colors.white)

              ],
            ),
          ),
          GestureDetector(
            onTap: (){
              Navigator.push(context, MaterialPageRoute(builder: (context)=> helpline()));
            },
            child: Container(
              color: primaryBlack,
              padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
              margin: EdgeInsets.symmetric(vertical: 5,horizontal: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text('HELPLINE',style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 20),),
                  Icon(Icons.call,color:Colors.white)

                ],
              ),
            ),
          ),
          Container(
            color: primaryBlack,
            padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
            margin: EdgeInsets.symmetric(vertical: 5,horizontal: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text('MYTH BUSTERS',style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 20),),
                Icon(Icons.arrow_forward,color:Colors.white)

              ],
            ),
          )
        ],
      ),
    );
  }
}
