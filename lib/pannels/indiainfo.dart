import 'package:flutter/material.dart';
class indiainfo extends StatelessWidget {
  final indiaData;

  const indiainfo({Key key, this.indiaData}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    String covidData(data){
      if(data>=1000000000){
        data = (data/1000000000).toStringAsFixed(2) + " B";
      }else if(data>1000000){
        data = (data/1000000).toStringAsFixed(2) + " M";

      }else if(data>1000){
        data = (data/1000).toStringAsFixed(0) + " K";
      }

      return data;
    }

    return Container(
      child: GridView(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2, childAspectRatio: 2),
        children: <Widget>[

          StatusPanel(
            title: 'CONFIRMED',
            panelColor: Colors.redAccent,
            textColor: Colors.white,
            count : covidData(indiaData['confirmed_cases']).toString(),
            iconVisible: "false",

          ),
          StatusPanel(
            title: 'ACTIVE',
            panelColor: Colors.blueAccent,
            textColor: Colors.white,
            count : covidData(indiaData['active_cases']).toString(),
            iconVisible: "true",
            popupDataTitle: "Active Rate",
            popupdata2:indiaData['last_total_death_cases'].toString(),
            popupdata1: indiaData['active_rate'].toString(),
            popupDataTitle2:"Last Active",


          ),
          StatusPanel(
            title: 'RECOVERED',
            panelColor: Colors.green,
            textColor: Colors.white,
            count : covidData(indiaData['active_cases']).toString(),
            iconVisible: "true",
            popupDataTitle: "Rate",
            popupDataTitle2: "Last Recovered",
            popupdata1: indiaData['recovered_rate'].toString() ,
            popupdata2: indiaData['last_total_recovered_cases'].toString() ,


          ),
          StatusPanel(
            title: 'DEATHS',
            panelColor: Colors.blueGrey,
            textColor: Colors.white,
            count : covidData(indiaData['death_cases']).toString(),
            iconVisible: "true",
            popupDataTitle: "Death Rate",
            popupDataTitle2: "Last Deaths",
            popupdata1: indiaData['death_rate'].toString() ,
            popupdata2: indiaData['last_total_death_cases'].toString(),



          ),



        ],
      ),
    );
  }
}
class StatusPanel extends StatelessWidget {
  final Color panelColor;
  final Color textColor;
  final String title;
  final String count;
  final String popupdata1;
  final String popupdata2;
  final String popupDataTitle;
  final String iconVisible;
  final String popupDataTitle2;



  const StatusPanel({Key key, this.panelColor, this.textColor, this.title, this.count, this.popupDataTitle,this.iconVisible, this.popupdata1, this.popupdata2, this.popupDataTitle2}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    //Dialog
    Widget DialogTxt(BuildContext context,String alertTitle1,String alertTitle2, String alertData1,String alertData2){
      var alertdialog = AlertDialog(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(alertTitle1,
              style: TextStyle(
                fontSize: 18,
                color: Colors.white,
              ) ,),

            Text(alertTitle2,style: TextStyle(color: Colors.white) ,),
          ],
        ),
        backgroundColor: Colors.blueGrey,

        content: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(alertData1,style: TextStyle(

              color: Colors.white

            )),
            Text(alertData2, style: TextStyle(color: Colors.white),),
          ],
        ),);
      showDialog(context: context,
          builder: (BuildContext context){
            return alertdialog;
          });
    }
    bool VisibilityIcon(String visibilityTxt){
      if(visibilityTxt == null){
        return true;
      }else if(visibilityTxt=="true"){
        return true;
      }else if(visibilityTxt == "false"){
        return false;
      }
    }
    return Container(
      margin: EdgeInsets.symmetric(vertical: 2,horizontal: 2),
      height: 40,width: width/2,

      decoration: BoxDecoration(
          color: panelColor,
          boxShadow: [
            BoxShadow(
                color: Colors.white,
                blurRadius: 25.0,
                spreadRadius: 5.0,
                offset: Offset(
                  15.0,
                  15.0,
                )
            )
          ]

      ),

      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[

          Container(
            padding: EdgeInsets.symmetric(vertical: 1, horizontal: 10),
            margin: EdgeInsets.symmetric(vertical: 5,horizontal: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,

              children: <Widget>[
                Text(title, style: TextStyle(color:textColor,fontWeight: FontWeight.bold, fontSize: 14,) ,),
                SizedBox(width: 10,),
                GestureDetector(
                  onTap: (){
                    DialogTxt(context, popupDataTitle, popupDataTitle2,popupdata1, popupdata2);
                  },
                    child: Visibility(
                      visible: VisibilityIcon(iconVisible),
                        child: Icon(Icons.info,color:Colors.white,)))
              ],
            ),
          ),



          Text(count, style: TextStyle(color:textColor,fontSize: 29, fontWeight: FontWeight.bold),),
          SizedBox(height: 5,),

        ],
      ),
    );
  }
}

