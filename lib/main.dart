import 'package:flutter/material.dart';
import 'package:flutterapp1/datasource.dart';
import 'package:flutterapp1/homepage.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

void main(){
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    theme: ThemeData(
      fontFamily: 'circular',
      primaryColor: primaryBlack

    ),
    home: HomePage(),
  ));
}
