import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutterapp1/datasource.dart';
import 'package:http/http.dart' as http;
import 'package:auto_size_text/auto_size_text.dart';
class helpline extends StatefulWidget {
  @override
  _helplineState createState() => _helplineState();
}

class _helplineState extends State<helpline> {
  List helplineData;
  var helpfinedData;
  int lengthLine;
  fetchHelpData()async{
    http.Response responseHelplineData = await http.get('https://covid-19india-api.herokuapp.com/v2.0/helpline_numbers');
    setState(() {
      helplineData = json.decode(responseHelplineData.body);
      helpfinedData = helplineData[[1][0]];
      lengthLine = helpfinedData['contact_details'].length;




    });
  }
   @override
  void initState() {
    // TODO: implement initState
     fetchHelpData();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(title: Text("Helpline"),),
      body:helpfinedData==null?Center(child: CircularProgressIndicator(),):
      ListView.builder(
          itemCount: lengthLine,
          itemBuilder: (context,index){
            return ExpansionTile(
              title: Text(helpfinedData['contact_details'][index]['state_or_UT'].toString(),style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold),),
              children: <Widget>[

                Container(
                  margin: EdgeInsets.symmetric(vertical: 10,horizontal: 10),
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[

                        AutoSizeText(
                    helpfinedData['contact_details'][index]['helpline_number'].toString(),
                    style: TextStyle(fontSize: 23),
                          maxLines: 1,
                          overflow: TextOverflow.fade,
                          minFontSize: 17,

                        ),

                        Icon(Icons.call,size: 40,),
                      ],
                    ),

                  ),
                ),

              ],
            );
          }),
    );
  }
}

