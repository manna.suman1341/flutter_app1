import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutterapp1/datasource.dart';
import 'package:flutterapp1/pannels/indiainfo.dart';
import 'package:flutterapp1/pannels/infopanel.dart';
import 'package:flutterapp1/pannels/worldwidepannel.dart';
import 'package:http/http.dart' as http;
class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  Map worldData;
  fetchworldData()async {
    http.Response response = await http.get("https://corona.lmao.ninja/v2/all");
    setState(() {
      worldData = json.decode(response.body);
    });
  }

  var countryData;

  fetchCountryData()async{
    http.Response responseCountryData = await http.get('https://covid19-api.org/api/prediction/IN');
    setState(() {
      countryData = json.decode(responseCountryData.body);



    });
  }

  List IndiaFetchData;
  var IndiaData;
  fetchIndiaData()async{
    http.Response responseIndiaData = await http.get("https://covid-19india-api.herokuapp.com/v2.0/country_data");
    setState(() {
      IndiaFetchData = json.decode(responseIndiaData.body);
      IndiaData = IndiaFetchData[[1][0]];
      print(IndiaData['active_cases']);

    });
  }

  @override
  void initState() {
    // TODO: implement initState
    fetchworldData();
    fetchCountryData();
    fetchIndiaData();

    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Covid 19 Tracker'),
      ),
body: SingleChildScrollView(child:Column(
  crossAxisAlignment: CrossAxisAlignment.start,
  children: <Widget>[
    Container(
      height: 100,
      color: Colors.orange[100],
      alignment: Alignment.center,
      padding: EdgeInsets.all(16),
      child:Text(DataSource.quote,style: TextStyle(color: Colors.orange[800],fontWeight: FontWeight.bold,fontSize: 14),)


    ),
    Padding(
      padding: const EdgeInsets.symmetric(vertical:10.0,horizontal: 30),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text('WorldWide', style: TextStyle(fontWeight: FontWeight.bold,fontSize: 22),),
          Container(

              padding: EdgeInsets.all(10),
              decoration: BoxDecoration(
                color: primaryBlack,
                borderRadius: BorderRadius.circular(16)
              ),
              child: Text('Regional', style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16,color: Colors.white),)),
        ],
      ),
    ),
    worldData== null?CircularProgressIndicator():worldWidePanel(WorldData:worldData),
    Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 9.0 ),
      child: Text('Covid 19 | India', style: TextStyle(fontWeight: FontWeight.bold,fontSize: 22),),
    ),

    IndiaData == null?CircularProgressIndicator():indiainfo(indiaData: IndiaData,),

//    countryData,worldData==null? Container() : MostEffectedPanel(countryData: countryData, actualcase: worldData, ),
    infopanel(),
    SizedBox(height: 20,),
    Center(
      child: Text("WE ARE TOGETHER IN FIGHT", style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16),),
    ),
    SizedBox(height: 20,),
  ],
))
    );
  }
}
